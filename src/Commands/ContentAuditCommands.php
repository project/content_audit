<?php

namespace Drupal\content_audit\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drush\Commands\DrushCommands;

/**
 * Audit tool to list entity schemas, volumes, and entity summaries.
 */
class ContentAuditCommands extends DrushCommands {

  /**
   * Provide metrics for the volume of entities.
   *
   * @command contentaudit:metrics
   *
   * @option with-bundles
   *   Itemise the counts per-bundle.
   *
   * @field-labels
   *   type: Entity category
   *   name: ID
   *   label: Label
   *   bundle_name: Bundle
   *   bundle_label: Bundle label
   *   count: Count
   *
   * @bootstrap full
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Volumetric results.
   */
  public function metrics($options = [
    'with-bundles' => FALSE,
    'format' => 'table',
  ]) {
    $etm = $this->entityTypeManager();

    $types = $this->doListEntityTypes();

    $output = [];
    if ($options['with-bundles']) {
      foreach ($types as $type) {
        if ($type['bundle_key']) {
          foreach ($this->listEntityBundles($type['name']) as $bundle => $label) {
            $row = $type;
            $row['bundle_name'] = $bundle;
            $row['bundle_label'] = $label;
            $row['count'] = $etm
              ->getStorage($type['name'])
              ->getQuery()
              ->condition($type['bundle_key'], $bundle)
              ->count()
              ->accessCheck(FALSE)
              ->execute();
            $output[] = $row;
          }
        }
        else {
          $row = $type;
          $row['bundle_name'] = NULL;
          $row['bundle_label'] = NULL;
          $row['count'] = $etm
            ->getStorage($type['name'])
            ->getQuery()
            ->count()
            ->accessCheck(FALSE)
            ->execute();
          $output[] = $row;
        }
      }
    }
    else {
      $output = array_map(function ($type) use ($etm) {
        $type['count'] = $etm
          ->getStorage($type['name'])
          ->getQuery()
          ->count()
          ->accessCheck(FALSE)
          ->execute();

        return $type;
      }, $types);
    }

    return new RowsOfFields($output);
  }

  /**
   * List the entity-types.
   *
   * @command contentaudit:list
   *
   * @field-labels
   *   type: Entity category
   *   name: ID
   *   label: Label
   *   fieldable: Fieldable
   *
   * @bootstrap full
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Field definitions for the given entity.
   */
  public function entityTypeListing() {
    $types = array_map(function ($type) {
      $type['fieldable'] = ($type['fieldable']) ? 'Yes' : 'No';
      return $type;
    }, $this->doListEntityTypes());

    return new RowsOfFields($types);
  }

  /**
   * Print the schema for all fieldable content entities.
   *
   * @command contentaudit:schema:all
   *
   * @field-labels
   *   entityType: Entity type
   *   bundle: Bundle ID
   *   provider: Provider
   *   name: Field ID
   *   label: Label
   *   type: Field type
   *   schema_type: DB schema type
   *   schema_size: DB schema size
   *
   * @bootstrap full
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Field definitions for the given entity.
   */
  public function schemaPrintAll($options = ['format' => 'table']) {
    $output = [];
    $entityIds = array_keys($this->listEntityTypes('content'));
    foreach ($entityIds as $entity) {
      $bundleIds = array_keys($this->listEntityBundles($entity));
      foreach ($bundleIds as $bundle) {
        foreach ($this->entityFieldManager()->getFieldDefinitions($entity, $bundle) as $id => $defn) {
          $schema_type = $schema_size = '';

          $storage = method_exists($defn, 'getFieldStorageDefinition')
            ? $defn->getFieldStorageDefinition()
            : $defn;

          if ($defn->isComputed()) {
            $schema_type = '- computed field -';
          }
          else {
            [$schema_type, $schema_size] = $this->parseSchemaDef($storage->getSchema());
          }

          $output[] = [
            'entityType'  => $entity,
            'bundle'      => $bundle,
            'name'        => $id,
            'label'       => $defn->getLabel(),
            'type'        => $defn->getType(),
            'schema_type' => $schema_type,
            'schema_size' => $schema_size,
            'provider'    => $storage->getProvider(),
          ];
        }

      }
    }

    return new RowsOfFields($output);
  }

  /**
   * Print the schema for a content entity.
   *
   * @command contentaudit:schema
   *
   * @usage contentaudit:schema <entity> <bundle>
   *   Print the schema for the specified content entity. The entity must be
   *   "fieldable".
   *
   * @example contentaudit:schema user
   * @example contentaudit:schema node article
   *
   * @field-labels
   *   entityType: Entity type
   *   bundle: Bundle ID
   *   provider: Provider
   *   name: Field ID
   *   label: Label
   *   type: Field type
   *   schema_type: DB schema type
   *   schema_size: DB schema size
   *
   * @bootstrap full
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Field definitions for the given entity.
   */
  public function schemaPrint($entity = NULL, $bundle = NULL, $options = ['format' => 'table']) {
    if ($entity) {
      // Validate the entity is fieldable.
      $match = $this->doListEntityTypes()[$entity] ?? NULL;
      if (empty($match) || !$match['fieldable']) {
        unset($entity);
      }
    }

    if (!$entity) {
      $entity = $this->io()->choice('Select entity type', $this->listEntityTypes('content'));
    }

    if ($bundle) {
      if (!array_key_exists($bundle, $this->listEntityBundles($entity))) {
        unset($bundle);
      }
    }

    if (!$bundle) {
      $choices = $this->listEntityBundles($entity);
      if (count($choices) === 0) {
        throw new \Exception('Invalid entity: ' . $entity);
      }
      elseif (count($choices) === 1) {
        $bundle = key($choices);
      }
      else {
        $bundle = $this->io()->choice('Select a bundle', $choices);
      }
    }

    $output = [];
    foreach ($this->entityFieldManager()->getFieldDefinitions($entity, $bundle) as $id => $defn) {
      $schema_type = $schema_size = '';

      $storage = method_exists($defn, 'getFieldStorageDefinition')
        ? $defn->getFieldStorageDefinition()
        : $defn;

      if ($defn->isComputed()) {
        $schema_type = '- computed field -';
      }
      else {
        [$schema_type, $schema_size] = $this->parseSchemaDef($storage->getSchema());
      }

      $output[] = [
        'entityType'  => $entity,
        'bundle'      => $bundle,
        'name'        => $id,
        'label'       => $defn->getLabel(),
        'type'        => $defn->getType(),
        'schema_type' => $schema_type,
        'schema_size' => $schema_size,
        'provider'    => $storage->getProvider(),
      ];
    }

    return new RowsOfFields($output);
  }

  /**
   * Process a schema definition to identify the field-type and size.
   *
   * @param array $schema
   *   The field schema provided by the field/entity APIs.
   *
   * @return array
   *   [string $schema_type, string $schema_size].
   */
  protected function parseSchemaDef(array $schema) {
    $schema_type = $schema_size = '';

    $column = $schema['columns'];
    if (array_key_exists('target_id', $column)) {
      $ref = $column['target_id'];
    }
    elseif (array_key_exists('value', $column)) {
      $ref = $column['value'];
    }

    if (!empty($ref)) {
      $schema_type = $ref['type'];
      if (array_key_exists('size', $ref)) {
        $schema_size = $ref['size'];
      }
      elseif (array_key_exists('length', $ref)) {
        $schema_size = $ref['length'];
      }
    }
    return [$schema_type, $schema_size];
  }

  /**
   * Fetch a list of bundles for a given entity-type.
   *
   * @param string $entity_type
   *   The machine-name of the entity.
   *
   * @return array
   *   Array of human-readable bundle labels indexed by their machine name.
   */
  private function listEntityBundles($entity_type) {
    return array_map(function ($definition) {
      return $definition['label'];
    }, $this->entityTypeBundleInfo()->getBundleInfo($entity_type));
  }

  /**
   * Fetch a list of content entity types or config entity types.
   *
   * @param string $type
   *   (optional) Type of entity: either 'content' or 'configuration'.
   *   Content is selected by default.
   *
   * @return array
   *   Array of entity-type labels indexed by their machine name
   */
  private function listEntityTypes($type = 'content') {
    return array_map(function ($entity_type) {
      return $entity_type['label'];
    }, array_filter($this->doListEntityTypes(), function ($entity_type) use ($type) {
      return $entity_type['type'] === $type;
    }));
  }

  /**
   * Fetch a list of entity-types.
   *
   * @return array
   *   Array of entity-type information including machine-name, label, and
   *   type (either a content entity or a configuration entity).
   */
  private function doListEntityTypes() {
    $result = array_map(function (EntityTypeInterface $entityType) {
      return [
        'name'       => $entityType->id(),
        'label'      => (string) $entityType->getLabel(),
        'fieldable'  => (bool) $entityType->entityClassImplements(FieldableEntityInterface::class),
        'bundle_key' => $entityType->getKey('bundle'),
        'type'       => $entityType->getGroup(),
      ];
    }, $this->entityTypeManager()->getDefinitions());

    // Sort by category (content / configuration) then by entity-type ID.
    uasort($result, function ($a, $b) {
      if ($a['type'] !== $b['type']) {
        return $a['type'] <=> $b['type'];
      }
      return $a['name'] <=> $b['name'];
    });

    return $result;
  }

  /**
   * Get the entity-type bundle-info service.
   *
   * @return \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   *   The entity-type bundle-info service.
   */
  private function entityTypeBundleInfo() {
    return \Drupal::service('entity_type.bundle.info');
  }

  /**
   * Get the entity-type manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity-type manager service.
   */
  private function entityTypeManager() {
    return \Drupal::service('entity_type.manager');
  }

  /**
   * Get the entity field manager service.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   *   The entity field manager service.
   */
  private function entityFieldManager() {
    return \Drupal::service('entity_field.manager');
  }

}
